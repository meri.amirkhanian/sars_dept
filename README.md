# ‘Sars’ Real Estate Agency department portal

The project is intented to build a department portal for a non-existing 'Sars' real estate agency. The primary backe-end programming language is Python 3.8 (Django Framework) with JS, CSS and HTML for front-end usage

The organisational structure can be represented as following:

![org](/documentation/sars_structure.png)

## Start the project on your local machine

1. To run the project first clone it to your computer by running:

`git clone https://gitlab.com/meri.amirkhanian/sars_dept.git`

2. Make sure you're on the project directory:

`cd sars_dept`

3. Finally run the following command which will give you the link to localhost:

`python manage.py runserver`

## Project Features

* User authentication: registration, log in, log out
* Password reset with e-mail request
* Django Rest Framework API
* Viewing separate pages of Department, Employee, Manager and Board Member details in table form
* Filtering the mentioned above record on multiple fields
* Contact page (for sharing work space issues with HR managers, for details see Use Cases below)
* SQL-supported dynamic calculation of average salary and number of employees for each department 

## Data Models

The project implies 4 data models:

 * Department
 
 
 * Employee

 
 * Manager

 
 * Board Member


The class relationship can be visualised with the following UML diagram:

![uml](/documentation/class_relationship.png)


## Use Cases

### Use Case 1 'Register'

__Primary Actor:__ Employee (Not a registered User)

__Preconditions:__

A new Employee is not yet registered to the department portal

__Brief:__

The Employee clicks on 'Register' button, fills out username, email, enters password 2 times.

__Postconditions:__

Minimal Guarantees: A user registered to the system. After that they are redirected to the login page to enter username and password
Success Guarantees: A user can see the contents of the web application

### Use Case 2 'Log Out'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is registered to the department portal

__Brief:__

The User clicks on 'Log Out' button and becomes logged out of the system. 

__Postconditions:__

Minimal Guarantees: A user is logged out the system
Success Guarantees: A user is logged out the system

### Use Case 3 'Log In'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged out of the department portal

__Brief:__

The User clicks on 'Log In' button, enters username and password and becomes logged in

__Postconditions:__

Minimal Guarantees: A user is logged in to the system
Success Guarantees: A user can see the contents of the web application


### Use Case 4 'Reset Password'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged out of the department portal, but forgot the password to login

__Brief:__

The User clicks on 'Forgot Password?' button, enters their e-mail. An e-mail is sent to them with a link to the page where they can enter a new password. The user enters the mentioned data and becomes able to login again

__Postconditions:__

Minimal Guarantees: An e-mail is sent to a user with a link to the page where they can enter a new password.
Success Guarantees: A user got a new password and logged in to the portal


### Use Case 5 'Forgot Password'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged out of the department portal, but forgot the password to login

__Brief:__

The User clicks on 'Forgot Password?' button, enters their e-mail. An e-mail is sent to them with a link to the page where they can enter a new password. The user enters the mentioned data and becomes able to login again

__Postconditions:__

Minimal Guarantees: An e-mail is sent to a user with a link to the page where they can enter a new password.
Success Guarantees: A user got a new password and logged in to the portal

### Use Case 6 'Looking for Information'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged in to the department portal

__Brief:__

The User wants to look for information about their colleagues, departments, managers and board members

__Postconditions:__

Minimal Guarantees: The user has the information available at the website
Success Guarantees: The user has satisfied their interest

### Use Case 7 'Workspace Issue'

__Primary Actor:__ Employee/Manager (A registered User)

__Preconditions:__

A User is logged in to the department portal and has an issue at the workplace

__Brief:__

The User clicks on 'Contact' button, enters their full name, e-mail, describes their issue. Then they click on 'Submit' button and become redirected to the page where it says that the HR manager will promptly respond to their issue

__Postconditions:__

Minimal Guarantees: The user has filled out the contact form
Success Guarantees: The email was sent to the HR manager

### Use Case 8 'Profile Update'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged in to the department portal and wants to update their profile

__Brief:__ 

The User clicks on 'Profile' button, and based on their priority, edits their first name, last name, e-mail and username. Then they click on 'Edit' button and become redirected to their updated profile page where it says that the profile edit was successful

__Postconditions:__

Minimal Guarantees: The user has entered edited personal data
Success Guarantees: The personal data edition was applied successfully





