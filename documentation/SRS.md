# Software Requirements Specification for ‘Sars’ Real Estate Agency department portal

 
## 1.	Introduction

### 1.1	Purpose 

The purpose of this document is to present a detailed description of the ‘Sars’ Real Estate Agency’s Department website. It will explain the purpose and features of the system, the interfaces of the system, what the system will do, the constraints under which it must operate and how the system will react to external stimuli. This document is intended for both the stakeholders and the developers of the system and will be proposed to the ‘Sars’ CEO for its approval.

### 1.2  Scope

This software specification applies to the whole project and implies a department website for ‘Sars’ real estate agency. This system will be designed to store data of departments, employees, managers and board members  

## 2	System Environment

![org](/documentation/sars_structure.png)

Figure 1 – System Environment

The department system has 4 roles: CEO (role – ‘ceo’) of ‘Sars’ real estate agency, who is the owner of the database and is granted with all the privileges, Board Members: CCO, CSO and CFO (role – ‘board_member’) each responsible for one of the 3 key operational parts of the company: commerce, service and finance who have a large span of privileges and Managers (role – ‘manager’) running the company 11 departments, who have limited privileges for the database. Also there will Employee role with sole Usage privilege, so that they will be able to only read the data (without access to the admin page)

### 2.1	Product Perspective

The product is supposed to be utilized by the management of ‘Sars’ real estate agency i. e. CEO, CCO, CSO and CFO and department managers. It is a web based system implementing client-server model. The Sars department portal provides simple mechanism for users to share and acquire knowledge. The system does not have predecessors, it will be created from scratch using company data on departments and employees.


### 2.2	Product Features

The following are the main features included in Sars department portal:

*User authentication: registration, log in, log out
*Password reset with e-mail request
*Django Rest Framework API
*Viewing separate pages of Department, Employee, Manager and Board Member details in table form
*Filtering the mentioned above record on multiple fields
*Contact page (for sharing work space issues with HR managers, for details see Use Cases below)
*SQL-supported dynamic calculation of average salary and number of employees for each department

### 2.3	Data Models and Table Views

4 buttons of the navigation bar of the department portal will redirect to table views of the specified data models. There are 4 models to be applied:

 * Department
 
| Data Item    |   Type     |   Description	Comment |
| ----------- | ----------- |-----------------------|
| id          | Integer     | Primary key, autoincremented
| name        | Varchar (50) | Name of the specific department
| board_member| Integer     |Id of the responsible Board Member, Foreign key
| manager     | Integer   | Id of the department Manager, Foreign key

 
 * Employee

| Data Item    |   Type     |   Description	Comment |
| ----------- | ----------- |-----------------------|
| id          | Integer     | Primary key, autoincremented
| name        | Varchar (50) | Name of Employee
| board_member| Integer     |Id of the responsible Board Member, Foreign key
| manager     | Integer   | Id of the department Manager, Foreign key
| department         | Integer   | Id of the department, Foreign key
| birth_date   | Date      | Date of birth 
| salary      | Decimal (19, 3)| Annual salary in thousand US dollars 
| e_mail       | Varchar (50) |  E-mail address
| phone_numb   | Varchar (13) |	 Phone number

 
 * Manager

| Data Item    |   Type     |   Description	Comment |
| ----------- | ----------- |-----------------------|
| id          | Integer     | Primary key, autoincremented
| name        | Varchar (50) | Name of the Manager
| board_member | Integer     |Id of the responsible Board Member,	Foreign key
| department   | Integer   | Id of the department, Foreign key
| birth_date   | Date      | Date of birth 
| salary      | Decimal (19, 3)| Annual salary in thousand US dollars 
| e_mail       | Varchar (50) |  E-mail address
| phone_numb   | Varchar (13) |	 Phone number
 
 * Board Member
 
| Data Item    |   Type     |   Description	Comment |
| ----------- | ----------- |-----------------------|
| id          | Integer     | Primary key, autoincremented
| name        | Varchar (50) | Name of the Board Member
| title        | Varchar (4) | Title of the Board Member
| birth_date   | Date      | Date of birth 
| salary      | Decimal (19, 3)| Annual salary in thousand US dollars 
| e_mail       | Varchar (50) |  E-mail address
| phone_numb   | Varchar (13) |	 Phone number
 
 The class relationship can be visualised with the following UML diagram:
 
 ![uml](/documentation/class_relationship.png)
 
 Figure 2 – Class Relationship
 
### 2.4	Design Constraints

#### 2.4.1 Software Languages

* Python 3.8
* JavaScript
* HTML
* CSS

#### 2.4.2 Architecture

Django Rest Framework

## 3. Use Cases

###3.1	Use Case 1 'Register'

__Primary Actor:__ Employee (Not a registered User)

__Preconditions:__

A new Employee is not yet registered to the department portal

__Brief:__

The Employee clicks on 'Register' button, fills out username, email, enters password 2 times.

__Postconditions:__

Minimal Guarantees: A user registered to the system. After that they are redirected to the login page to enter username and password
Success Guarantees: A user can see the contents of the web application

### 3.2	Use Case 2 'Log Out'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is registered to the department portal

__Brief:__

The User clicks on 'Log Out' button and becomes logged out of the system. 

__Postconditions:__

Minimal Guarantees: A user is logged out the system
Success Guarantees: A user is logged out the system

### 3.3	Use Case 3 'Log In'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged out of the department portal

__Brief:__

The User clicks on 'Log In' button, enters username and password and becomes logged in

__Postconditions:__

Minimal Guarantees: A user is logged in to the system
Success Guarantees: A user can see the contents of the web application


### 3.4	Use Case 4 'Reset Password'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged out of the department portal, but forgot the password to login

__Brief:__

The User clicks on 'Forgot Password?' button, enters their e-mail. An e-mail is sent to them with a link to the page where they can enter a new password. The user enters the mentioned data and becomes able to login again

__Postconditions:__

Minimal Guarantees: An e-mail is sent to a user with a link to the page where they can enter a new password.
Success Guarantees: A user got a new password and logged in to the portal


### 3.5	Use Case 5 'Forgot Password'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged out of the department portal, but forgot the password to login

__Brief:__

The User clicks on 'Forgot Password?' button, enters their e-mail. An e-mail is sent to them with a link to the page where they can enter a new password. The user enters the mentioned data and becomes able to login again

__Postconditions:__

Minimal Guarantees: An e-mail is sent to a user with a link to the page where they can enter a new password.
Success Guarantees: A user got a new password and logged in to the portal

### 3.6	Use Case 6 'Looking for Information'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

A User is logged in to the department portal

__Brief:__

The User wants to look for information about their colleagues, departments, managers and board members

__Postconditions:__

Minimal Guarantees: The user has the information available at the website
Success Guarantees: The user has satisfied their interest

### 3.7	Use Case 7 'Workspace Issue'

__Primary Actor:__ Employee/Manager (A registered User)

__Preconditions:__

A User is logged in to the department portal and has an issue at the workplace

__Brief:__

The User clicks on 'Contact' button, enters their full name, e-mail, describes their issue. Then they click on 'Submit' button and become redirected to the page where it says that the HR manager will promptly respond to their issue

__Postconditions:__

Minimal Guarantees: The user has filled out the contact form
Success Guarantees: The email was sent to the HR manager

### 3.8	Use Case 8 'Profile Update'

__Primary Actor:__ Employee/Manager/Board Member (A registered User)

__Preconditions:__

__A User is logged in to the department portal and wants to update their profile

__Brief:__ 

The User clicks on 'Profile' button, and based on their priority, edits their first name, last name, e-mail and username. Then they click on 'Edit' button and become redirected to their updated profile page where it says that the profile edit was successful

__Postconditions:__

Minimal Guarantees: The user has entered edited personal data
Success Guarantees: The personal data edition was applied successfully



