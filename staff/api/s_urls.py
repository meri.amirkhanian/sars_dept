from django.urls import path, include

from staff.api.s_views import EmployeeDetail, EmployeeView, DepartmentView, DepartmentDetail, ManagerView, ManagerDetail, BoardMemberView, BoardMemberDetail

app_name = 'staff'


urlpatterns = [
    # path('register', registration_view, name='register'),
    # path('employees/', EmployeeView.as_view()),
    # path('employees/<int:id>', EmployeeView.as_view()),
    path('employees/', EmployeeView.as_view()),
    path('employees/<int:pk>', EmployeeDetail.as_view()),
    path('departments/', DepartmentView.as_view()),
    path('departments/<int:pk>', DepartmentDetail.as_view()),
    path('managers/', ManagerView.as_view()),
    path('managers/<int:pk>', ManagerDetail.as_view()),
    path('board_members/', BoardMemberView.as_view()),
    path('board_members/<int:pk>', BoardMemberDetail.as_view()),
    path('departments/', DepartmentView.as_view()),
    path('managers/', ManagerView.as_view()),
    path('board-members/', BoardMemberView.as_view()),
]