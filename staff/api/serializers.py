from rest_framework import serializers
from staff.models import Employee, Department, Manager, BoardMember


class BoardMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = BoardMember
        fields = ['id', 'bm_name', 'title', 'salary', 'birth_date', 'e_mail', 'phone_numb']


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ['id', 'd_name']


class ManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manager
        fields = ['id',  'm_name', 'salary', 'birth_date', 'e_mail', 'phone_numb']


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ['id',  'e_name', 'salary', 'birth_date', 'e_mail', 'phone_numb']
