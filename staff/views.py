from django.views.decorators.cache import never_cache
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import *
from django.core.mail import send_mail
from django.views.generic import ListView


def home_page(request):
    return render(request, 'home.html', {})


@login_required
def contact(request):
    if request.method == 'POST':
        message_name = request.POST['message_name']
        message = request.POST['message']
        message_email = request.POST['message_email']
        send_mail(
            'Workspace issue from ' + message_name,
            message_email  + message,
            message_email,
            ['meri.amirkhanian@gmail.com'],
            fail_silently = False)

        return render(request, 'contact2.html', {'message_name': message_name})
    else:
        return render(request, 'contact.html', {})


@login_required
def emp_dept_join(request):
    cursor = connection.cursor()
    cursor.execute(
        'select employees.id,employees.e_name, employees.board_member, board_members.bm_name, departments.d_name, employees.salary,'
        'employees.e_mail, employees.phone_numb, employees.birth_date, managers.m_name, managers.id, bm_name from employees '
        'join departments on employees.department=departments.id  '
        'join board_members on employees.board_member=board_members.id '
        'join managers on employees.manager=managers.id;')
    results = cursor.fetchall()

    return render(request, 'employees.html', {'Employee': results})


@login_required
def dept_m_join(request):
    cursor = connection.cursor()
    cursor.execute('select employees.department, d.d_name, m.m_name, b.bm_name, ROUND(AVG(employees.salary), 3)'
                   'as avg_salary, COUNT(e_name) as empl_count from employees '
                   'join departments d on employees.department = d.id '
                   'join board_members b on employees.board_member = b.id '
                   'join managers m on employees.manager = m.id '
                   'group by employees.department, m.m_name, b.bm_name' )
    results1 = cursor.fetchall()
    return render(request, 'departments.html', {'Department': results1})


@login_required
def manage_bm_join(request):
    cursor = connection.cursor()
    cursor.execute('select managers.id,managers.m_name, bm_name, departments.d_name, managers.salary, managers.e_mail, '
                   'managers.phone_numb, managers.birth_date, board_members.bm_name from managers '
                   'join departments on managers.department=departments.id  '
                   'join board_members on departments.board_member=board_members.id;')

    results2 = cursor.fetchall()
    return render(request, 'managers.html', {'Managers': results2})


@login_required
def bm(request):
    display = BoardMember.objects.all()
    return render(request, 'board_members.html', {'BoardMember': display})


@never_cache
def handler404(request, *args, **kwargs):
    return render(request, '404.html', status=404)


@never_cache
def handler500(request):
    return django.views.defaults.server_error(request, '500.html')