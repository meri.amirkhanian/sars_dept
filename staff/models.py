from django.db import connection
from django.db import models


class BoardMember(models.Model):
    id = models.IntegerField(primary_key=True)
    bm_name = models.CharField(max_length=100)
    salary = models.DecimalField(max_digits=9, decimal_places=3)
    title = models.CharField(max_length=100)
    birth_date = models.DateField(max_length=100)
    e_mail = models.EmailField(max_length=100)
    phone_numb = models.CharField(max_length=100)

    class Meta:
        db_table = 'board_members'

    def __str__(self):
        return self.bm_name


class Department(models.Model):
    id = models.IntegerField(primary_key=True)
    # board_member = models.ForeignKey(BoardMember, null=True, on_delete=models.SET_NULL)
    d_name = models.CharField(max_length=100)

    class Meta:
        db_table = 'departments'

    def __str__(self):
        return self.d_name


class Manager(models.Model):
    id = models.IntegerField(primary_key=True)
    # department = models.ForeignKey(Department, null=True, on_delete=models.SET_NULL)
    # board_member = models.ForeignKey(BoardMember, null=True, on_delete=models.SET_NULL)
    m_name = models.CharField(max_length=100)
    salary = models.DecimalField(max_digits=9, decimal_places=3)
    birth_date = models.DateField(max_length=100)
    e_mail = models.EmailField(max_length=100)
    phone_numb = models.CharField(max_length=100)

    class Meta:
        db_table = 'managers'

    def __str__(self):
        return self.m_name


class Employee(models.Model):
    id = models.IntegerField(primary_key=True)
    e_name = models.CharField(max_length=100)
    # department = models.ForeignKey(Department, null=True, on_delete=models.SET_NULL)
    # manager = models.ForeignKey(Manager, null=True, on_delete=models.SET_NULL)
    # board_member = models.ForeignKey(BoardMember, null=True, on_delete=models.SET_NULL)
    salary = models.DecimalField(max_digits=9, decimal_places=3)
    birth_date = models.DateField(auto_created=False)
    e_mail = models.CharField(max_length=100)
    phone_numb = models.CharField(max_length=100)


    class Meta:
        db_table = 'employees'

    def __str__(self):
        return self.e_name
