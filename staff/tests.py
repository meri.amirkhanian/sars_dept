import unittest
import pytest
from django.contrib.auth.models import User
from django.test.client import Client
from staff import views
from django.test import TestCase, Client, SimpleTestCase
from django.shortcuts import reverse
from sars_dept_portal.urls import urlpatterns
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from staff.models import Employee, Department, Manager, BoardMember
from django.apps import apps
from django.test import TestCase
from staff.apps import StaffConfig
from users.apps import UsersConfig
from django.test import TestCase


class ReportsConfigTest(TestCase):
    def test_staff(self):
        self.assertEqual(StaffConfig.name, 'staff')
        self.assertEqual(apps.get_app_config('staff').name, 'staff')

    def test_users(self):
        self.assertEqual(UsersConfig.name, 'users')
        self.assertEqual(apps.get_app_config('users').name, 'users')


class ErrorsTest(TestCase):
    def test_handler_404(self):
        response = self.client.get('/something/really/weird/')
        self.assertEqual(response.status_code, 404)


class TestHomePage(SimpleTestCase):

    def test_home_status(self):

        response = self.client.get('http://127.0.0.1:8000/')
        self.assertEquals(response.status_code, 200)

    def test_home_url_name(self):

        response = self.client.get(reverse('home'))
        self.assertEquals(response.status_code, 200)

    def test_correct_template(self):
        response = self.client.get(reverse('home'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')


class TestBMsPage(TestCase):

    def testLogin(self):
        self.client = Client()
        self.client.login(username='kasper', password='employee2025')

    def test_bm_status(self):
        response = self.client.get('/board_members')
        self.assertEqual(response.status_code, 301)

    def test_empl_status(self):
        response = self.client.get('/employees')
        self.assertEqual(response.status_code, 301)

    def test_dept_status(self):
        response = self.client.get('/departments')
        self.assertEqual(response.status_code, 301)

    def test_man_status(self):
        response = self.client.get('/managers')
        self.assertEqual(response.status_code, 301)

    def testLogout(self):
        self.client.logout()
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, 200)

    def test_404(self):
        resp = self.client.get('/made_up_url/')
        self.assertEqual(resp.status_code, 404)


class TestBModel(unittest.TestCase):

    def set_upp(self, id=5, bm_name='testbm', salary=34.999, title='untitled', birth_date='1990-09-09',
                                   e_mail='nomail@gmail.com', phone_numb='(095)5343775'):
        return BoardMember.objects.create(id=id, bm_name=bm_name, salary=salary, title=title, birth_date=birth_date,
                                   e_mail=e_mail, phone_numb=phone_numb)

    def test_bm_name(self):
        testbm = self.set_upp()
        self.assertTrue(isinstance(testbm, BoardMember ))
        self.assertEquals(testbm.__str__(), testbm.bm_name)


class BaseUser(TestCase):

    def create_user(self):
        username, password = 'admin', 'test'
        user = User.objects.get_or_create(
            username=username,
            email='admin@test.com',
            is_superuser=True)[0]
        user.set_password(password)
        user.save()
        self.user = user
        return (username, password)

    def login(self):
        username, password = self.create_user()
        self.client.login(username=username, password=password)

    def setUp(self):
        self.user = get_user_model().objects.create_user(username='test',
                                                         password='12test12',
                                                         email='test@example.com')
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_correct(self):
        user = authenticate(username='test', password='12test12')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='wrong', password='12test12')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_password(self):
        user = authenticate(username='test', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)
