from django.contrib import admin
from staff.models import Employee, Department, Manager, BoardMember

admin.site.register(Employee)
admin.site.register(Department)
admin.site.register(Manager)
admin.site.register(BoardMember)


