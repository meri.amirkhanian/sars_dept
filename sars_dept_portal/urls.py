from django.contrib import admin
from staff import views
from users import views as user_views
from django.urls import path, include
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', user_views.register, name='register'),
    path('profile/', user_views.profile, name='profile'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='password_reset.html'), name='password_reset'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete'),
    path('password-reset/done', auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'), name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html'), name='password_reset_confirm'),
    path('api/', include('staff.api.s_urls')),
    path('employees/', views.emp_dept_join, name='employees'),
    path('departments/', views.dept_m_join, name='departments'),
    path('managers/', views.manage_bm_join, name='managers'),
    path('board_members/', views.bm, name='bm'),
    path('contact/', views.contact, name='contact'),
    path('', views.home_page, name='home'),
]

handler404 = 'staff.views.handler404'
handler500 = 'staff.views.handler500'
